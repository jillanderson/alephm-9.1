#pragma once

/** \file **********************************************************************
 *
 * sterowanie podłączaniem zespołów głośnikowych do końcówki mocy
 *
 ******************************************************************************/

#include <stdbool.h>

/*
 * inicjalizacja sterowania głośnikami
 */
void hw_speakers_init(void);

/*
 * podłącza głośniki
 */
void hw_speakers_on(void);

/*
 * odłącza głośniki
 */
void hw_speakers_off(void);

/*
 * sprawdza czy głośniki są podłączone
 *
 * @ret		true - głośniki podłączone
 *		false - głośniki niepodłączone
 */
bool hw_speakers_is_on(void);

