#pragma once

/** \file **********************************************************************
 *
 * sterowanie zasilaniem
 *
 ******************************************************************************/

#include <stdbool.h>

/*
 * inicjuje sterowanie zasilaniem
 */
void hw_supply_init(void);

/*
 * włącza zasilanie urządzenia
 */
void hw_supply_on(void);

/*
 * wyłącza zasilanie urządzenia
 */
void hw_supply_off(void);

/*
 * sprawdza czy zasilanie jest włączone
 *
 * @ret		true - zasilanie włączone
 *		false - zasilanie wyłączone
 */
bool hw_supply_is_on(void);

