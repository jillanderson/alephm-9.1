#pragma once

/** \file **********************************************************************
 *
 * obsługa przycisku
 *
 ******************************************************************************/

#include <stdbool.h>

/*
 * inicjalizuje obsługę przycisku
 */
void hw_key_init(void);

/*
 * sprawdza stan przycisku
 * przycisk w stanie aktywnym (wciśnięty) zwiera pin do masy
 *
 * @ret		true - przycisk wciśnięty
 *		false - przycisk zwolniony
 */
bool hw_key_is_pressed(void);

