#include "../speakers.h"

#include <avr/io.h>
#include <mega/io.h>
#include <stdbool.h>

static io_pin_st const RELAY_SPEAKER_LEFT = { .port = &PORTC, .pin = PC4 };
static io_pin_st const RELAY_SPEAKER_RIGHT = { .port = &PORTC, .pin = PC5 };

static bool speaker_left_is_on(void);
static bool speaker_right_is_on(void);

void hw_speakers_init(void)
{
	io_configure_as_output(&RELAY_SPEAKER_LEFT);
	io_configure_as_output(&RELAY_SPEAKER_RIGHT);
	io_set_low(&RELAY_SPEAKER_LEFT);
	io_set_low(&RELAY_SPEAKER_RIGHT);
}

void hw_speakers_on(void)
{
	io_set_high(&RELAY_SPEAKER_LEFT);
	io_set_high(&RELAY_SPEAKER_RIGHT);
}

void hw_speakers_off(void)
{
	io_set_low(&RELAY_SPEAKER_LEFT);
	io_set_low(&RELAY_SPEAKER_RIGHT);
}

bool hw_speakers_is_on(void)
{
	return (((true == speaker_left_is_on())
	                && (true == speaker_right_is_on())) ? true : false);
}

bool speaker_left_is_on(void)
{
	return ((0U == io_read_pin(&RELAY_SPEAKER_LEFT)) ? false : true);
}

bool speaker_right_is_on(void)
{
	return ((0U == io_read_pin(&RELAY_SPEAKER_RIGHT)) ? false : true);
}
