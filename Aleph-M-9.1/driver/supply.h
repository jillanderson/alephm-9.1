#pragma once

/** \file **********************************************************************
 *
 * sterowanie zasilaniem
 *
 ******************************************************************************/

/*
 * inicjuje sterowanie zasilaniem
 */
void supply_init(void);

/*
 * włącza zasilanie urządzenia
 */
void supply_on(void);

/*
 * wyłącza zasilanie urządzenia
 */
void supply_off(void);

