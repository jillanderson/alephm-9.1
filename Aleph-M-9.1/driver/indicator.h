#pragma once

/** \file **********************************************************************
 *
 * sterowanie wskaźnikiem optycznym w przycisku zasilania
 *
 ******************************************************************************/

#include <stdint.h>

/*
 * inicjalizacja wskaźnika
 */
void indicator_init(void);

/*
 * włącza wskaźnik
 */
void indicator_on(void);

/*
 * wyłącza wskaźnik
 */
void indicator_off(void);

/*
 * włącza wskaźnik w tryb błyskania z zadanymi czasami włączenia
 * i wyłączenia wskaźnika
 *
 * @on_time	czas włączenia
 * @off_time	czas wyłączenia
 */
void indicator_blink(uint16_t on_time, uint16_t off_time);

/*
 * odlicza jednostki czasu
 * funkcja powinna być wywoływana cyklicznie, okres pomiędzy wywołaniami
 * tej funkcji definiuje jednostkę czasu
 */
void indicator_on_tick_time(void);

