#pragma once

/** \file **********************************************************************
 *
 * sterowanie podłączaniem zespołów głośnikowych do końcówki mocy
 *
 ******************************************************************************/

#include <stdint.h>

/*
 * inicjalizacja sterowania głośnikami
 */
void speakers_init(void);

/*
 * przyłącza głośniki z zadaną zwłoką czasową
 *
 * @latency_ms	czas opóźnienia przyłączenia głośników
 */
void speakers_connect(uint16_t latency_ms);

/*
 * odłącza głośniki bezzwłocznie
 */
void speakers_disconnect(void);

/*
 * odlicza jednostki czasu
 * funkcja powinna być wywoływana cyklicznie, okres pomiędzy wywołaniami
 * tej funkcji definiuje jednostkę czasu
 */
void speakers_on_tick_time(void);

