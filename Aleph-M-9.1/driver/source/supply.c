#include "../../hal/supply.h"
#include "../supply.h"

void supply_init(void)
{
	hw_supply_init();
}

void supply_on(void)
{
	hw_supply_on();
}

void supply_off(void)
{
	hw_supply_off();
}

