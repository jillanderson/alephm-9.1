#include "../../hal/speakers.h"
#include "../speakers.h"

#include <stdbool.h>
#include <stdint.h>

static struct {
	volatile uint16_t latency_counter;
	volatile enum {
		COUNTER_STOPPED,
		COUNTER_RUNNING
	} counter_status;
} speakers = { 0 };

void speakers_init(void)
{
	hw_speakers_init();
}

void speakers_connect(uint16_t latency_ms)
{
	if (COUNTER_STOPPED == speakers.counter_status) {
		if (false == hw_speakers_is_on()) {
			speakers.latency_counter = latency_ms;
			speakers.counter_status = COUNTER_RUNNING;
		}
	}
}

void speakers_disconnect(void)
{
	hw_speakers_off();
	speakers.counter_status = COUNTER_STOPPED;
}

void speakers_on_tick_time(void)
{
	if (COUNTER_RUNNING == speakers.counter_status) {

		if (0U < speakers.latency_counter) {
			speakers.latency_counter--;
		}

		if (0U == speakers.latency_counter) {
			hw_speakers_on();
			speakers.counter_status = COUNTER_STOPPED;
		}
	}
}

